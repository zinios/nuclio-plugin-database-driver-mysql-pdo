<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\driver\mysqlPDO
{
	use nuclio\core\ClassManager;
	use nuclio\plugin\database\
	{
		common\CommonCursorInterface,
		common\DBOrder,
		common\DBRecord
	};
	use \PDO;
	use \PDOStatement;
	
	<<
	provides('database::mysql::cursor'),
	factory
	>>
	class Cursor implements CommonCursorInterface
	{
		private int $numRecords				=0;
		private Vector<DBRecord> $records	=Vector{};
		private int $position				=0;
		private PDOStatement $cursor;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Cursor
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(PDOStatement $cursor)
		{
			$this->cursor		=$cursor;
			$this->numRecords	=$cursor->rowCount();
			$this->records		=new Vector($this->cursor->fetchAll(PDO::FETCH_ASSOC));
		}
		
		public function current():?DBRecord
		{
			$current=$this->records->get($this->position);
			if (!is_null($current))
			{
				return new Map($current);
			}
			return null;
		}
		
		public function key():int
		{
			return $this->position;
		}
		
		public function next():void
		{
			$this->position++;
		}
		
		public function rewind():void
		{
			$this->position=0;
		}
		
		public function valid():bool
		{
			return !is_null($this->current());
		}
		
		public function seek(int $position):void
		{
			$this->position=$position;
		}
		
		public function count():int
		{
			return $this->numRecords;
		}
		
		public function limit(int $limit):this
		{
			// $this->cursor->limit($limit);
			return $this;
		}
		
		public function offset(int $offset):this
		{
			// $this->cursor->offset($offset);
			return $this;
		}
		
		public function orderBy(DBOrder $order):this
		{
			
			return $this;
		}
	}
}
