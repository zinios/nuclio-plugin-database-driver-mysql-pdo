Release Notes
-------------
2.0.3
-----
* Fixed a issue with MySQL Cursor not returning a valid response from valid().

2.0.2
-----
* Fixed return type on MySQL::buildColumn.

2.0.1
-----
* Removed duplicate case in switch.

2.0.0
-----
* [BC Break] We no longer create tables JIT.
* New method MySQL::alterColumn with 3 preset modes: add, remove, restore for use when attempting to safely remove a column without destroying data.
* New method MySQL::columnExists for testing if a column has already been created.
* MySQL::buildColumn is now a public method.

1.0.2
-----
* Fixed PDO path.


1.0.1
-----
* Fixed package name.


1.0.0
-----
* Initial Release.
